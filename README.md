# Taller 1

- Desarrollar un simulador de gravedad con JS, CSS y HTML teniendo en cuenta los siguientes parámetros:

## Requirements

1. Utilizar las funciones `setInterval` y `setTimeout`.
2. Crear un menú que permita elegir el planeta a simular.
3. Mostrar en pantalla el planeta seleccionado.
4. Presentar un objeto que permita observar la caída libre.
5. Al hacer clic sobre el objeto, se deberá activar la animación de caída libre de acuerdo al planeta seleccionado.
6. Subir el repositorio con el código del proyecto a GitHub o GitLab.
7. Compartir el enlace del repositorio en el Aula.


## Ejemplo de referencia

> https://phet.colorado.edu/sims/html/gravity-and-orbits/latest/gravity-and-orbits_all.html?locale=es